#!/bin/bash
#
# Disabling Firewall and SELinux, things that usually causing troubles.
#
# This script requires root access.

# Disable Firewall
systemctl disable --now firewalld
systemctl mask firewalld

# Disable SELinux
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
