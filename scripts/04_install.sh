#!/bin/bash
#
# Updating base image (Feb 13th). Installing Java and other dependencies for
# Oracle. Finally installing Oracle 19c Database.
#
# Troubleshoots:
# You can install everything with:
#    rpm -Uvh --nodeps --force *.rpm
# But it probably break stuff, due to order of things being installed and other
# things missing and expected to exists.

RPMS_LOCATION='/tmp/files/rpms/'

cd "${RPMS_LOCATION}"
# Updates
# To get the files, use:
#    yum update --downloadonly --downloaddir=<PATH TO SAVE RPMS>
#    [files/rpms/01_init/]
yum localinstall -y 01_update/* 

# Packages require by Oracle.
# To get the files, be sure to be in a path where Oracle RPMs exists and all
# updates already applied (use localinstall on the downloaded files):
#    yum localinstall --downloadonly --downloaddir=<PATH TO SAVE RPMS> oracle-database-*
# After you've installed Oracle dependencies, download Java:
#    yum install java-1.8.0-openjdk-devel --downloadonly --downloaddir=<PATH TO SAVE RPMS>
#    [files/rpms/02_oracle-dep/]
yum localinstall -y 02_oracle-dep/*

# Installing Oracle 19c Database + preinstall
yum localinstall -y 03_oracle/*
