#!/bin/bash
#
# Configuring Oracle DB

# Creating 'oracle' user home and updating update '.bashrc'
mkhomedir_helper oracle
echo "export ORACLE_BASE=/opt/oracle" >> /home/oracle/.bashrc
echo "export ORACLE_HOME=/opt/oracle/product/19c/dbhome_1" >> /home/oracle/.bashrc
echo "export ORACLE_SID=ORCLCDB" >> /home/oracle/.bashrc
echo "export PATH=\$PATH:/opt/oracle/product/19c/dbhome_1/bin" >> /home/oracle/.bashrc

# Fixing ip address/hostnam
sed -i -e "/$MY_HOSTNAME/d" /etc/hosts
echo "${INTERNAL_IP1} ${MY_HOSTNAME}" >> /etc/hosts

# Configuring default Oracle database
bash -x /etc/init.d/oracledb_ORCLCDB-19c configure

# You can test the DB is running with
#   sudo su - oracle
#   lsnrctl status
