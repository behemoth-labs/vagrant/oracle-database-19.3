#!/bin/bash
#
# To generate this file, go to the parent directory, and run:
#    find . -type f -iname '*.rpm' -exec sha256sum {} \; > rpms.sha256

RPMS_LOCATION='/tmp/files/rpms/'

cd "${RPMS_LOCATION}"
sha256sum -c ../texts/rpms.sha256

if [ $? -ne 0 ]; then
    echo "!!! Missing or Modified files !!!"
    echo "!!! Missing or Modified files !!!"
    echo "!!! Missing or Modified files !!!"
    echo "!!! Missing or Modified files !!!"
    echo "!!! Missing or Modified files !!!"
fi
