#!/bin/bash
#
# Subscribing to Red Hat service (for updates).
# Growing the disk to the maximum size.
#
# This script requires root access.

# # echo 'Subscribing with RHEL...'
# subscription-manager register --username ${RHEL_SUB_USER} --password ${RHEL_SUB_PASS} \
#     --auto-attach

# Installing 'growpart' package, to enable disk growing.
# To downloading this file, use:
#    yum localinstall --downloadonly --downloaddir=<PATH TO SAVE RPMS> cloud-utils-growpart
#    [files/rpms/00_init/]
rpm -ivh /tmp/cloud-utils-growpart-*.noarch.rpm

# Update disk size (grow)
growpart /dev/vda 2
lvextend -l +100%FREE /dev/mapper/rhel-root
xfs_growfs /dev/mapper/rhel-root
df -h
