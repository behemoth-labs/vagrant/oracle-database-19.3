# Generating a has for Validation

To generate this file, go to the parent directory, and run:

```
find . -type f -iname '*.rpm' -exec sha256sum {} \; > rpms.sha256
```
