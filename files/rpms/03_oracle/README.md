# Required Packages

You will need to download manually from Oracle's site the following files:

```
oracle-database-preinstall-19c-1.0-2.el8.x86_64.rpm
oracle-database-ee-19c-1.0-1.x86_64.rpm
```
