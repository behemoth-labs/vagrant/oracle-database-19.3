# Required Package(s)

To get the files, be sure to be in a path where Oracle RPMs exists and all updates already applied (use localinstall on the downloaded files):

```
yum localinstall --downloadonly --downloaddir=<PATH TO SAVE RPMS> oracle-database-*
```
After you've installed Oracle dependencies, download Java:

```
yum install java-1.8.0-openjdk-devel --downloadonly --downloaddir=<PATH TO SAVE RPMS>
```
