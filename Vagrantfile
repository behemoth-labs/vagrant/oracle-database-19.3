# -*- mode: ruby -*-
# vi: set ft=ruby :

# Variables and secrets
require 'yaml'
params = YAML.load_file 'variables.yaml'
secrets = YAML.load_file 'secrets.yaml'

servers=[
    {
        # Cloudera Manager
        :hostname => params['oracle']['hostname'],
        :ip1 => params['oracle']['ip1'],
        :box => params['global']['box_name'],
        :box_json => params['global']['box_json'],
        :ram => params['oracle']['memory'],
        :cpu => params['oracle']['cores'],
        :disk_size => params['oracle']['root_disk'],
        :additional_disks => params['oracle']['additional_disks'],
        :role => params['oracle']['role']
      }
]

Vagrant.configure("2") do |config|
    # Resize ens5's mtu to '1392'.
    # This is an internal issue due to WireGuard VPN.
    config.vm.provision "shell",
        run: "always",
        inline: "sudo ip link set dev ens5 mtu 1392"

    # Have to disable 'rsync', as it tries to install packages
    # and fail before the MTU was fixed for my local issue.
    config.nfs.verify_installed = false
    config.vm.synced_folder './sync', '/vagrant', type: 'rsync', disabled: true

    # Don't inject pub key
    config.ssh.insert_key = false
    config.ssh.username = "vagrant"
    config.ssh.password = 'vagrant'

    # Loop creating VMs
    servers.each do |machine|
        config.vm.define machine[:hostname] do |node|
            config.vm.box = machine[:box]
            config.vm.box_url = [ machine[:box_json ] ]
            node.vm.hostname = machine[:hostname]

            # Private
            node.vm.network :private_network,
                :ip => machine[:ip1]

            node.vm.provider :libvirt do |lv|
                lv.storage_pool_name = params["global"]["pool_name"]
                lv.machine_virtual_size = machine[:disk_size]
                lv.qemu_use_session = false
                lv.cpus = machine[:cpu]
                lv.driver = "kvm"
                lv.memory = machine[:ram]
                lv.nic_model_type = "virtio"
                lv.nested = true
            end

            node.vm.provision "file", source: "files/rpms/00_init/cloud-utils-growpart-0.33-0.el8.noarch.rpm", destination: "/tmp/cloud-utils-growpart-0.33-0.el8.noarch.rpm"
            node.vm.provision "shell", path: "scripts/01_init.sh", privileged: true, run: "once", env: {"RHEL_SUB_PASS" => secrets['global']['rhel_password'], "RHEL_SUB_USER" => secrets['global']['rhel_username'] }
            node.vm.provision "file", source: "files", destination: "/tmp/"
            node.vm.provision "shell", path: "scripts/02_disable.sh", privileged: true, run: "once"
            node.vm.provision "shell", path: "scripts/03_validate.sh", privileged: false
            node.vm.provision "shell", path: "scripts/04_install.sh", privileged: true
            node.vm.provision "shell", path: "scripts/05_configure.sh", privileged: true, run: "once", env: {
                "MY_HOSTNAME"  => params['oracle']['hostname'],
                "INTERNAL_IP1" => params['oracle']['ip1']
            }
        end
    end

    # config.trigger.before :destroy do |trigger|
    #     trigger.name = "Making sure to unregister from RHEL"
    #     trigger.run_remote = { inline: "sudo subscription-manager unregister" }
    #     trigger.on_error = :halt
    # end
end

#Guide: https://docs.oracle.com/en/database/oracle/oracle-database/19/ladbi/running-rpm-packages-to-install-oracle-database.html#GUID-BB7C11E3-D385-4A2F-9EAF-75F4F0AACF02

#https://github.com/oracle/vagrant-projects/blob/main/OracleDatabase/19.3.0/Vagrantfile
