# Vagrant, Installing Oracle DB 19.3c with RPMs

In this Vagrant deployment, we'll create a single Oracle 19.3c Database. The installation is done with RPMs, and this guide is loosly based on [Oracle's Github repo of vagrant](https://github.com/oracle/vagrant-projects/blob/main/OracleDatabase/19.3.0/Vagrantfile).

## Requirements

- Vagrant (2.3.4)
- Libvirt, KVM and QEMU.
- Storage => 40GB.
- Memory => 4GB.
- You own, pre-made Red Hat 8.8 Box.
- If you wish to update the RPMs,
  - Internet Access (for dev only, will not be required in the final "production" version).
  - Redhat Subscription.

## Box

This lab is using a custom Box, based on Red Hat Enterprise Linux 8.8. The json file should point to the location **you** have the box file at, and have the correct hash value, for example:

```
{
    "name": "rhel8_8-10",
    "description": "Red Hat Enterprise Linux 8.8",
    "versions": [{
        "version": "8.8-10",
        "providers": [{
            "name": "libvirt",
            "url": "file:///home/itzhak/Downloads/BOXes/rhel8.8/rhel8.8-10.box",
            "checksum": "2c6c6f7cdad0fae06d36b616e440ba20f505f1272dcfc89aaabbc996aece13ab",
            "checksum_type": "sha256"
        }]
    }]
} 

```

## Variables & Secrets

You will need a `variables.yaml` file which hold the variables used when creating the VM, while these variables aren't considered secret, they may differ from place to place - the default ones may not work for you. Due to this fact, this file isn't included, and you will need to create it for yourself.

`variables.yaml`
```
---
global:
  box_name: "rhel8_8"
  box_json: "rhel8_8.json"
  pool_name: "default"

oracle:
  role: "oracle"
  hostname: "oracle-db"
  ip1: "192.168.30.99"
  memory: 8096
  cores: 2
  root_disk: 50
---
```

You will also need a secret files, `secret.yaml`, even if you don't planned to use it, it is required, so just make a template. This file holds the credentials for Red Hat Access.

`secrets.yaml`
```
---
global:
  rhel_username: "<USERNAME>"
  rhel_password: "<PASSWORD>"
---
```

## Scripts & Files

The lab creation is based on scripts under the `scripts` directory and files under the `files` directory. The files you will need to collect/create yourself, follow the scripts and place the files in the corresponding path under files.

### Oracle RPMs

The Oracle RPMs can be downloaded from their website, you will need an account in order to download anything from there, it's free. You will need two files, make sure to place them under `03_oracle`.

```
oracle-database-preinstall-19c-1.0-2.el8.x86_64.rpm
oracle-database-ee-19c-1.0-1.x86_64.rpm
```

### Validation

Under `files/texts` you will need to create a hash for all the RPMs you have, in order to make sure the files are correct. See under the scripts directory `03_validate.sh` how to generate that file.

## Running

Don't forget, this lab is based on a custom box! Also, you're required to manually fetch them and place them in the right place. Without these requirements, the lab will not work.

```
vagrant validate
vagrant up
```

## Shutdown

When using `vagrant destroy`, the script is using a `config.trigger.before :destroy` to unsubsribe from Redhat. In some cases, when the provisioning failed, or when the box wasn't destroyed successfully, the subscription will remain and you will be required to manually remove it and try again.
